package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.Contact;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ContactUnitTest {
    @Test
    public void setIdTest() throws NoSuchFieldException, IllegalAccessException {
        long value = 111111111;
        Contact instance = new Contact();
        instance.setId(value);
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), 111111111L);
    }

    @Test
    public void setEmailTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "test@test.com";
        Contact instance = new Contact();
        instance.setEmail(value);
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), "test@test.com");
    }

    @Test
    public void setPhoneTest() throws NoSuchFieldException, IllegalAccessException {
        String value = "987654321";
        Contact instance = new Contact();
        instance.setPhone(value);
        final Field field = instance.getClass().getDeclaredField("phone");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), "987654321");
    }

    @Test
    public void setAddressTest() throws NoSuchFieldException, IllegalAccessException {
        Address value = new Address("Avda. Universidad", "Caceres", "Caceres", "10003", "Espania");
        Contact instance = new Contact();
        instance.setAddress(value);
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getIdTest() throws NoSuchFieldException, IllegalAccessException {
        final Contact instance = new Contact();
        final Field field = instance.getClass().getDeclaredField("id");
        field.setAccessible(true);
        field.set(instance, 111111111L);

        //when
        final long result = instance.getId();

        //then
        assertEquals("field wasn't retrieved properly", result, 111111111L);
    }

    @Test
    public void getEmailTest() throws NoSuchFieldException, IllegalAccessException {
        final Contact instance = new Contact();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "test@test.com");

        //when
        final String result = instance.getEmail();

        //then
        assertEquals("field wasn't retrieved properly", result, "test@test.com");
    }

    @Test
    public void getPhoneTest() throws NoSuchFieldException, IllegalAccessException {
        final Contact instance = new Contact();
        final Field field = instance.getClass().getDeclaredField("phone");
        field.setAccessible(true);
        field.set(instance, "987654321");

        //when
        final String result = instance.getPhone();

        //then
        assertEquals("field wasn't retrieved properly", result, "987654321");
    }

    @Test
    public void getAddressTest() throws NoSuchFieldException, IllegalAccessException {
        final Contact instance = new Contact();
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        Address value = new Address("Avda. Universidad", "Caceres", "Caceres", "10003", "Espania");
        field.set(instance, value);

        //when
        final Address result = instance.getAddress();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }
}
