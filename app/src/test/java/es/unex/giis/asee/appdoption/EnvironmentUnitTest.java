package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Environment;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class EnvironmentUnitTest {
    @Test
    public void setChildenTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = "children_test";
        Environment instance = new Environment();
        instance.setChildren(value);
        final Field field = instance.getClass().getDeclaredField("children");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setDogsTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = "dogs_test";
        Environment instance = new Environment();
        instance.setDogs(value);
        final Field field = instance.getClass().getDeclaredField("dogs");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setCatsTest() throws NoSuchFieldException, IllegalAccessException {
        Object value = "cats_test";
        Environment instance = new Environment();
        instance.setCats(value);
        final Field field = instance.getClass().getDeclaredField("cats");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getChildrenTest() throws NoSuchFieldException, IllegalAccessException {
        final Environment instance = new Environment();
        final Field field = instance.getClass().getDeclaredField("children");
        field.setAccessible(true);
        field.set(instance, "children_test");

        //when
        final Object result = instance.getChildren();

        //then
        assertEquals("field wasn't retrieved properly", result, (Object)"children_test");
    }

    @Test
    public void getDogsTest() throws NoSuchFieldException, IllegalAccessException {
        final Environment instance = new Environment();
        final Field field = instance.getClass().getDeclaredField("dogs");
        field.setAccessible(true);
        field.set(instance, "dogs_test");

        //when
        final Object result = instance.getDogs();

        //then
        assertEquals("field wasn't retrieved properly", result, (Object)"dogs_test");
    }

    @Test
    public void getCatsTest() throws NoSuchFieldException, IllegalAccessException {
        final Environment instance = new Environment();
        final Field field = instance.getClass().getDeclaredField("cats");
        field.setAccessible(true);
        field.set(instance, "cats_test");

        //when
        final Object result = instance.getCats();

        //then
        assertEquals("field wasn't retrieved properly", result, (Object)"cats_test");
    }
}
