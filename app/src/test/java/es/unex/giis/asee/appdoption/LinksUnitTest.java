package es.unex.giis.asee.appdoption;

import org.junit.Test;

import java.lang.reflect.Field;

import es.unex.giis.asee.appdoption.Model.Links;
import es.unex.giis.asee.appdoption.Model.Self;
import es.unex.giis.asee.appdoption.Model.Type;
import es.unex.giis.asee.appdoption.Model.Organization;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class LinksUnitTest {
    @Test
    public void setSelfTest() throws NoSuchFieldException, IllegalAccessException {
        Self value = new Self();
        value.setHref("prueba_self");
        Links instance = new Links();
        instance.setSelf(value);
        final Field field = instance.getClass().getDeclaredField("self");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setTypeTest() throws NoSuchFieldException, IllegalAccessException {
        Type value = new Type();
        value.setHref("prueba_type");
        Links instance = new Links();
        instance.setType(value);
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setOrganizationTest() throws NoSuchFieldException, IllegalAccessException {
        Organization value = new Organization();
        value.setHref("prueba_organization");
        Links instance = new Links();
        instance.setOrganization(value);
        final Field field = instance.getClass().getDeclaredField("organization");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSelfTest() throws NoSuchFieldException, IllegalAccessException {
        final Links instance = new Links();
        final Field field = instance.getClass().getDeclaredField("self");
        field.setAccessible(true);
        Self value = new Self();
        value.setHref("prueba_self");
        field.set(instance, value);

        //when
        final Self result = instance.getSelf();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void getTypeTest() throws NoSuchFieldException, IllegalAccessException {
        final Links instance = new Links();
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        Type value = new Type();
        value.setHref("prueba_type");
        field.set(instance, value);

        //when
        final Type result = instance.getType();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }

    @Test
    public void getOrganizationTest() throws NoSuchFieldException, IllegalAccessException {
        final Links instance = new Links();
        final Field field = instance.getClass().getDeclaredField("organization");
        field.setAccessible(true);
        Organization value = new Organization();
        value.setHref("prueba_organization");
        field.set(instance, value);

        //when
        final Organization result = instance.getOrganization();

        //then
        assertEquals("field wasn't retrieved properly", result, value);
    }
}
