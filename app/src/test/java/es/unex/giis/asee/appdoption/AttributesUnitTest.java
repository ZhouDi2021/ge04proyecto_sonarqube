package es.unex.giis.asee.appdoption;

import static org.junit.Assert.assertEquals;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.junit.Test;

import java.lang.reflect.Field;

public class AttributesUnitTest {


    @Test
    public void setSpayedNeuteredTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setSpayedNeutered(value);
        final Field field = instance.getClass().getDeclaredField("spayedNeutered");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setHouseTrainedTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setHouseTrained(value);
        final Field field = instance.getClass().getDeclaredField("houseTrained");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }


    @Test
    public void setDeclawedTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setDeclawed(value);
        final Field field = instance.getClass().getDeclaredField("declawed");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setSpecialNeedsTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setSpecialNeeds(value);
        final Field field = instance.getClass().getDeclaredField("specialNeeds");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setShotCurrentTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setShotsCurrent(value);
        final Field field = instance.getClass().getDeclaredField("shotsCurrent");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void setShotsCurrentTest() throws NoSuchFieldException, IllegalAccessException {
        Boolean value = true;
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        instance.setSpayedNeutered(value);
        final Field field = instance.getClass().getDeclaredField("spayedNeutered");
        field.setAccessible(true);
        assertEquals("Fields didn't match", field.get(instance), value);
    }

    @Test
    public void getSpayedNeuteredTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        final Field field = instance.getClass().getDeclaredField("spayedNeutered");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getSpayedNeutered();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }


    @Test
    public void getHouseTrainedTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        final Field field = instance.getClass().getDeclaredField("houseTrained");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getHouseTrained();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }

    @Test
    public void getDeclawedTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        final Field field = instance.getClass().getDeclaredField("declawed");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getDeclawed();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }

    @Test
    public void getSpecialNeedsTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        final Field field = instance.getClass().getDeclaredField("specialNeeds");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getSpecialNeeds();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }

    @Test
    public void getShotsCurrentTest() throws NoSuchFieldException, IllegalAccessException {
        es.unex.giis.asee.appdoption.Model.Attributes instance = new es.unex.giis.asee.appdoption.Model.Attributes();
        final Field field = instance.getClass().getDeclaredField("shotsCurrent");
        field.setAccessible(true);
        field.set(instance, true);

        //when
        final boolean result = instance.getShotsCurrent();

        //then
        assertEquals("field wasn't retrieved properly", result, true);
    }


}
