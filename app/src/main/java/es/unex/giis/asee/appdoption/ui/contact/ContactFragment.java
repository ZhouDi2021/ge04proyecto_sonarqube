package es.unex.giis.asee.appdoption.ui.contact;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import es.unex.giis.asee.appdoption.Navigate;

import com.google.android.material.snackbar.Snackbar;

import es.unex.giis.asee.appdoption.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private View mView;
    private Context context;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        this.context=context;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ContactFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactFragment newInstance(String param1, String param2) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_contact, container, false);

        TextView ownerEmail = (TextView) mView.findViewById(R.id.owner_email);
        String email = getArguments().getString("email_contact");
        ownerEmail.setText(email);

        Button contactBtn = (Button) mView.findViewById(R.id.contactbutton);
        contactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailContent = ((TextView)mView.findViewById(R.id.contact_message)).getText().toString();
                String adopterPhone = ((TextView)mView.findViewById(R.id.adopter_phone)).getText().toString();


                Intent intent = new Intent (Intent.ACTION_SEND);      intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
                intent.putExtra(Intent.EXTRA_SUBJECT, "Interested in adopt");
                intent.putExtra(Intent.EXTRA_TEXT, emailContent+"\n"+"My telephone number is: "+adopterPhone);
                intent.setPackage("com.google.android.gm");
                if (intent.resolveActivity(context.getPackageManager())!=null)
                    startActivity(intent);
                else
                    Toast.makeText(getContext(),"Gmail App is not installed", Toast.LENGTH_SHORT).show();
            }
        });


        Button cancelBtn = (Button)mView.findViewById(R.id.cncl_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(mView,"Cancelar adopcion",Snackbar.LENGTH_LONG).show();
                NavDirections action = ContactFragmentDirections.actionNavContactToNavHome();
                ((Navigate)context).navigateTo(action);
            }
        });
        return mView;
    }
}