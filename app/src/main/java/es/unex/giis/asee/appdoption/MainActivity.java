package es.unex.giis.asee.appdoption;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import es.unex.giis.asee.appdoption.Model.Address;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.User;
import es.unex.giis.asee.appdoption.databinding.ActivityMainBinding;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;
import es.unex.giis.asee.appdoption.ui.favorites.FavouritesFragmentDirections;
import es.unex.giis.asee.appdoption.ui.home.HomeFragmentDirections;
import es.unex.giis.asee.appdoption.ui.pets.PetsFragmentDirections;

public class MainActivity extends AppCompatActivity implements MyAdapter.OnListIterationListener, Navigate {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;
    private NavController navActController;
    private User userDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_favorites, R.id.nav_incidence, R.id.nav_mypets,R.id.nav_aboutus)
                .setOpenableLayout(drawer)
                .build();
        navActController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navActController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navActController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onListInteraction(Animal animal, String from) {
        if(from.equals("home")) {
            NavDirections action = HomeFragmentDirections.actionNavHomeToNavPet(animal);
            navigateTo(action);
        }
        if(from.equals("mypets")){
            NavDirections action = PetsFragmentDirections.actionNavMypetsToUpdDelPet(animal);
            navigateTo(action);
        }
        if(from.equals("favourites")){
            NavDirections action = FavouritesFragmentDirections.actionNavFavoritesToNavPet(animal);
            navigateTo(action);
        }

    }

    public NavController getNavController(){
        return navActController;
    }


    @Override
    public void navigateTo(NavDirections action) {

        getNavController().navigate(action);
    }
}