package es.unex.giis.asee.appdoption.ui.home;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.LinkedList;
import java.util.List;
import es.unex.giis.asee.appdoption.AppExecutors;
import es.unex.giis.asee.appdoption.DependencyInjectors.AppContainer;
import es.unex.giis.asee.appdoption.DependencyInjectors.MyApplication;
import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.MyAdapter;
import es.unex.giis.asee.appdoption.Navigate;
import es.unex.giis.asee.appdoption.OnPetsLoadedListener;
import es.unex.giis.asee.appdoption.PetsNetworkLoaderRunnable;
import es.unex.giis.asee.appdoption.R;
import es.unex.giis.asee.appdoption.ViewModels.HomeFragmentViewModel;
import es.unex.giis.asee.appdoption.databinding.FragmentHomeBinding;

public class HomeFragment extends Fragment implements OnPetsLoadedListener{

    private FragmentHomeBinding binding;
    private RecyclerView mRecyclerView;
    private Context context;
    private MyAdapter myAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setHasOptionsMenu(true);
        FloatingActionButton fab = (FloatingActionButton)binding.getRoot().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavDirections action = HomeFragmentDirections.actionNavHomeToCreatePet2();
                ((Navigate)context).navigateTo(action);
            }
        });
        mRecyclerView = (RecyclerView) binding.getRoot().findViewById(R.id.petList);

        mRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridManager = new GridLayoutManager(getContext(),2);
        mRecyclerView.setLayoutManager(gridManager);
        myAdapter = new MyAdapter(new LinkedList<>(), (MyAdapter.OnListIterationListener) context,"home");
        mRecyclerView.setAdapter(myAdapter);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;

        HomeFragmentViewModel viewModel = new ViewModelProvider(this,appContainer.homefactory).get(HomeFragmentViewModel.class);
        viewModel.getHomePets().observe(getActivity(),this::onPetsLoaded);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                myAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                myAdapter.filter(newText);
                return true;
            }
        });
        super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public void onPetsLoaded(List<Animal> pets) {
        myAdapter.swap(pets);
    }
}