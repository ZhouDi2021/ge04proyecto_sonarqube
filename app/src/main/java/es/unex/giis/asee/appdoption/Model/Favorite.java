package es.unex.giis.asee.appdoption.Model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorites")
public class Favorite {
    @PrimaryKey(autoGenerate = true)
    private long fid;
    @ColumnInfo(name = "userid")
    private long userid;
    @Embedded
    private Animal animalfav;

    @Ignore
    public Favorite(long userid, Animal fav){
        this.userid=userid;
        this.animalfav = fav;
    }

    public Favorite(long fid, long userid, Animal animalfav) {
        this.fid = fid;
        this.userid = userid;
        this.animalfav = animalfav;
    }

    @Ignore
    public Favorite() {

    }

    public long getFid() {
        return fid;
    }

    public void setFid(long fid) {
        this.fid = fid;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public Animal getAnimalfav() {
        return animalfav;
    }

    public void setAnimalfav(Animal animalfav) {
        this.animalfav = animalfav;
    }
}
