package es.unex.giis.asee.appdoption;

import android.content.Context;

import java.util.List;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

public class PetsDiskLoaderRunnable implements Runnable{

    private final OnPetsLoadedListener mOnPetsLoadedListener;
    private Context context;

    public PetsDiskLoaderRunnable(OnPetsLoadedListener mOnPetsLoadedListener, Context context) {
        this.mOnPetsLoadedListener = mOnPetsLoadedListener;
        this.context=context;
    }

    @Override
    public void run() {
        List<Animal> animalsDB=null;
        AppDoptionDatabase db = AppDoptionDatabase.getInstace(context);

        List<Animal> finalAnimalsDB = animalsDB;
        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                mOnPetsLoadedListener.onPetsLoaded(finalAnimalsDB);
            }
        });
    }
}
