package es.unex.giis.asee.appdoption;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import es.unex.giis.asee.appdoption.Model.Animal;
import es.unex.giis.asee.appdoption.Model.Photo;
import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.PetHolder> {

    private List<Animal> mPets;
    private List<Animal> copyAnimals = new LinkedList<>(); //Necesaria para restaurar en la busqueda
    private OnListIterationListener mListener;
    private String from;

    public MyAdapter(List<Animal>pets, OnListIterationListener onListListener,String from){
        mPets=pets;
        mListener=onListListener;
        this.from=from;
    }

    @Override
    public PetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pet_list_view, parent,false);
        return new PetHolder(v, v.getContext(), mListener,this.from);
    }

    @Override
    public void onBindViewHolder(PetHolder holder, int position) {
        holder.pet=mPets.get(position);
        holder.bind(holder.pet);
    }

    @Override
    public int getItemCount() {
        return mPets.size();
    }


    public void swap(List<Animal> n_pets){
        mPets=n_pets;
        Log.i("NUMERO MASCOTAS",String.valueOf(mPets.size()));
        copyAnimals.addAll(n_pets);
        notifyDataSetChanged();
    }

    public static class PetHolder extends RecyclerView.ViewHolder{
        public Context context;
        public String from;
        public Animal pet;
        public TextView petName;
        public ImageView petImg;
        public TextView petBreed;

        public PetHolder(View itemView,Context context, OnListIterationListener listener,String from) {
            super(itemView);
            this.context = context;
            petName = itemView.findViewById(R.id.petName);
            petImg = itemView.findViewById(R.id.petImg);
            this.from=from;
           // petBreed = itemView.findViewById(R.id.petRaze);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OnListIterationListener on = (OnListIterationListener) context;
                    on.onListInteraction(pet,from);
                }
            });
        }

        public void bind(Animal animal){
            petName.setText(animal.getName());
          //  petBreed.setText("Breed: "+animal.getBreeds().getPrimary());
            if(animal.getPhoto()==null) {
                if (animal.getPhotos() != null) {
                    if (animal.getPhotos().size() != 0) {
                        //Puede ser que la imagen no esté en la primera posicion
                        Iterator<Photo> it = animal.getPhotos().iterator();
                        Photo p=it.next();
                        Picasso.with(context).load(p.getLarge()).error(R.drawable.dog).into(petImg);
                    }
                } else {
                    Picasso.with(context).load(R.drawable.samplemini).into(petImg);
                }
            }
            else{
                Picasso.with(context).load(pet.getPhoto()).error(R.drawable.samplemini).into(petImg);
            }
        }

    }


    public void add(Animal animal){
        mPets.add(animal);
        notifyDataSetChanged();
    }

    public interface OnListIterationListener{
        public void onListInteraction(Animal animal,String from);
    }

    public void filter(String text) {
        mPets.clear();
            if (text.isEmpty()) {
                mPets.addAll(copyAnimals);
            } else {
                text = text.toLowerCase();
                for (Animal item : copyAnimals) {
                    if (item.getName().toLowerCase().contains(text) || item.getStatus().toLowerCase().contains(text)) {
                        mPets.add(item);
                    }
                }
            }
        notifyDataSetChanged();
    }

}
