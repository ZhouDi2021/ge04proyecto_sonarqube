package es.unex.giis.asee.appdoption.DependencyInjectors;

import android.app.Application;

public class MyApplication extends Application {
    public AppContainer appContainer;//Contenedor de dependencias

    //Hay que sobreescribir aqui el metodo para poder obtener el contexto de la aplicacion
    @Override
    public void onCreate() {
        super.onCreate();
        //Necesito esperar a que Android nos proporcione el contexto
        appContainer = new AppContainer(this);
    }
}
