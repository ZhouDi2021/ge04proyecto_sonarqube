
package es.unex.giis.asee.appdoption.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Pet implements Serializable
{

    @SerializedName("animals")
    @Expose
    private List<Animal> animals;
    private final static long serialVersionUID = 3789110418269457847L;

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimal(List<Animal> animals) {
        this.animals = animals;
    }

}
