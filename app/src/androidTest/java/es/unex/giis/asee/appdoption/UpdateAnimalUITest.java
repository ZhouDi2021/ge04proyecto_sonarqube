package es.unex.giis.asee.appdoption;


import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import es.unex.giis.asee.appdoption.roomdb.AppDoptionDatabase;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class UpdateAnimalUITest {

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void updateAnimalUITest() {
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab),isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.animal_name),isDisplayed()));
        appCompatEditText.perform(replaceText("Test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.animal_breed), isDisplayed()));
        appCompatEditText2.perform(replaceText("test"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.animal_age),isDisplayed()));
        appCompatEditText3.perform(replaceText("test"), closeSoftKeyboard());

        ViewInteraction textInputEditText = onView(
                allOf(withId(R.id.animal_description),isDisplayed()));
        textInputEditText.perform(replaceText("test"), closeSoftKeyboard());

        ViewInteraction materialButton = onView(
                allOf(withId(R.id.send_btn), withText("PUBLICAR"),isDisplayed()));
        materialButton.perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.petList)));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.animal_name_delete),isDisplayed()));
        appCompatEditText4.perform(replaceText("Test2"));

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.animal_name_delete),isDisplayed()));
        appCompatEditText5.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        appCompatEditText6.perform(replaceText("No Adoptable"));

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        appCompatEditText7.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.animal_breed_delete),isDisplayed()));
        appCompatEditText8.perform(replaceText("test2"));

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.animal_breed_delete),isDisplayed()));
        appCompatEditText9.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText10 = onView(
                allOf(withId(R.id.animal_age_delete),isDisplayed()));
        appCompatEditText10.perform(replaceText("test2"));

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.animal_age_delete),isDisplayed()));
        appCompatEditText11.perform(closeSoftKeyboard());

        ViewInteraction textInputEditText2 = onView(
                allOf(withId(R.id.description_text),isDisplayed()));
        textInputEditText2.perform(replaceText("test2"));

        ViewInteraction textInputEditText3 = onView(
                allOf(withId(R.id.description_text),isDisplayed()));
        textInputEditText3.perform(closeSoftKeyboard());

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.send_btn),isDisplayed()));
        materialButton2.perform(click());

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.petList)));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction editText = onView(
                allOf(withId(R.id.animal_name_delete),isDisplayed()));
        editText.check(matches(withText("Test2")));

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.animal_breed_delete),isDisplayed()));
        editText2.check(matches(withText("test2")));

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.animal_age_delete),isDisplayed()));
        editText3.check(matches(withText("test2")));

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.animal_state_delete),isDisplayed()));
        editText4.check(matches(withText("No Adoptable")));

        ViewInteraction editText5 = onView(
                allOf(withId(R.id.description_text),isDisplayed()));
        editText5.check(matches(withText("test2")));
    }

    @AfterClass
    public static void restoreDB(){
        AppDoptionDatabase.getInstace(InstrumentationRegistry.getInstrumentation().getContext()).getAnimalDAO().deleteAllForTest();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
